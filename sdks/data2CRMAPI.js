'use strict';

var util = require('util');
var restletUtils = require('../restletUtils');
var securityUtils = require('../securityUtils');

/**
 * @class Data2CRMAPI
 * @param {string} [endpoint] - The API endpoint
 */
function Data2CRMAPI(endpoint) {
  if (restletUtils.isDefined(endpoint) && (!restletUtils.isString(endpoint) || restletUtils.isString(endpoint) && endpoint.length === 0)) {
    throw new Error('endpoint parameter must be a non-empty string.');
  }

  this.globalSecurity = {};
  this.securityConfigurations = {};
  this.endpoint = restletUtils.stripTrailingSlash(endpoint || 'https://api-2445581398133.apicast.io:443/v1');
}

/**
 * Sets up the authentication to be performed through API token
 *
 * @method
 * @name Data2CRMAPI#setApiToken
 * @param {string} tokenName - the name of the query parameter or header based on the location parameter.
 * @param {string} tokenValue - the value of the token
 * @param {string} location - the location of the token, either 'HEADER' or 'QUERY'.
 * If undefined it defaults to 'header'.
 */
Data2CRMAPI.prototype.configureGlobalApiToken = function(tokenName, tokenValue, location) {
  if (restletUtils.isUndefined(location)) {
    util.log('No location defined, it defaults to \'HEADER\'');
    location = 'HEADER';
  }

  if (location.toUpperCase() !== 'HEADER' && location.toUpperCase() !== 'QUERY') {
    throw new Error('Unknown location: ' + location);
  }

  this.globalSecurity = {
    type: 'API_KEY',
    placement: location.toUpperCase(),
    name: tokenName,
    token: tokenValue
  };
};

/**
 * Sets up the authentication to be performed through oAuth2 protocol
 * meaning that the Authorization header will contain a Bearer token.
 *
 * @method
 * @param token - the oAuth token to use
 */
Data2CRMAPI.prototype.configureGlobalOAuth2Token = function (token) {
  this.globalSecurity = {
    type: 'OAUTH2',
    token: 'Bearer ' + token
  };
};

/**
 * Sets up the authentication to be performed through basic auth.
 *
 * @method
 * @name Data2CRMAPI#setBasicAuth
 * @param {string} username - the user's username
 * @param {string} key - the user's key or password
 */
Data2CRMAPI.prototype.configureGlobalBasicAuthentication = function(username, key) {
  this.globalSecurity = {
    type: 'BASIC',
    token: 'Basic ' + new Buffer(username + ':' + key).toString('base64')
  };
};


/**
 * Returns all accounts from the system
 * @method
 * @name Data2CRMAPI#getAccountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.name - Name
 * @param {string} config.queryParameters.email - Email
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAccountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/account',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add account into the system
 * @method
 * @name Data2CRMAPI#createAccountEntity
 * @param {object} body - Add account into the system; is of type: AccountCreate; has the following structure:
{
  "account_category" : "General",
  "address_type" : "Billing",
  "annual_revenue" : 23244.43,
  "billing_city" : "Brandenburg",
  "billing_country" : "USA",
  "billing_state" : "NY",
  "billing_street" : "P.O. Box 300, 2698 Ultricies Road",
  "billing_zip" : "47212",
  "description" : "Some long description",
  "email" : "bill.wall@mail.com",
  "email_alt" : "bill.wall.alt@mail.com",
  "employees" : 230,
  "facebook" : "http://www.facebook.com/your_page",
  "fax" : "+49 9131 85 28732",
  "industry" : "Electronics",
  "livejournal" : "http://your_page.livejournal.com/",
  "name" : "Bill Wall",
  "owner" : "Nicky McCartney",
  "owner_id" : "21312411",
  "ownership" : "Public",
  "parent_id" : "21312411",
  "phone" : "(817) 569-8900",
  "phone_alt" : "(817) 569-8900",
  "rating" : "Active",
  "shipping_city" : "Brandenburg",
  "shipping_country" : "USA",
  "shipping_state" : "NY",
  "shipping_street" : "P.O. Box 300, 2698 Ultricies Road",
  "shipping_zip" : "47212",
  "sic_code" : "2895-1",
  "tickersymbol" : "%",
  "twitter" : "http://twitter.com/your_page",
  "type" : "Company",
  "website" : "http://google.com/",
  "website_alt" : "http://google.com/"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createAccountEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/account',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all accounts from the system
 * @method
 * @name Data2CRMAPI#getAccountCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAccountCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/account/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for accounts
 * @method
 * @name Data2CRMAPI#getAccountDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "account",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAccountDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/account/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return account information
 * @method
 * @name Data2CRMAPI#getAccountEntity
 * @param {string} account_id - REQUIRED - Account Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAccountEntity = function(account_id, config, callback) {
  restletUtils.checkPathVariables(account_id, 'account_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/account/' + account_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update account information
 * @method
 * @name Data2CRMAPI#updateAccountEntity
 * @param {string} account_id - REQUIRED - Account Identifier
 * @param {object} body - Update account information; is of type: AccountUpdate; has the following structure:
{
  "account_category" : "General",
  "address_type" : "Billing",
  "annual_revenue" : 23244.43,
  "billing_city" : "Brandenburg",
  "billing_country" : "USA",
  "billing_state" : "NY",
  "billing_street" : "P.O. Box 300, 2698 Ultricies Road",
  "billing_zip" : "47212",
  "description" : "Some long description",
  "email" : "bill.wall@mail.com",
  "email_alt" : "bill.wall.alt@mail.com",
  "employees" : 230,
  "facebook" : "http://www.facebook.com/your_page",
  "fax" : "+49 9131 85 28732",
  "industry" : "Electronics",
  "livejournal" : "http://your_page.livejournal.com/",
  "name" : "Bill Wall",
  "owner" : "Nicky McCartney",
  "owner_id" : "21312411",
  "ownership" : "Public",
  "parent_id" : "21312411",
  "phone" : "(817) 569-8900",
  "phone_alt" : "(817) 569-8900",
  "rating" : "Active",
  "shipping_city" : "Brandenburg",
  "shipping_country" : "USA",
  "shipping_state" : "NY",
  "shipping_street" : "P.O. Box 300, 2698 Ultricies Road",
  "shipping_zip" : "47212",
  "sic_code" : "2895-1",
  "tickersymbol" : "%",
  "twitter" : "http://twitter.com/your_page",
  "type" : "Company",
  "website" : "http://google.com/",
  "website_alt" : "http://google.com/"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateAccountEntity = function(account_id, body, config, callback) {
  restletUtils.checkPathVariables(account_id, 'account_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/account/' + account_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete account information
 * @method
 * @name Data2CRMAPI#deleteAccountEntity
 * @param {string} account_id - REQUIRED - Account Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteAccountEntity = function(account_id, config, callback) {
  restletUtils.checkPathVariables(account_id, 'account_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/account/' + account_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all attachments from the system
 * @method
 * @name Data2CRMAPI#getAttachmentCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.parent_type - Parent Type
 * @param {string} config.queryParameters.parent_id - Parent Identifier
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAttachmentCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/attachment',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add attachment into the system
 * @method
 * @name Data2CRMAPI#createAttachmentEntity
 * @param {object} body - Add attachment into the system; is of type: AttachmentCreate; has the following structure:
{
  "contact_name" : "Bill Wall",
  "created_at" : null,
  "description" : "Description of attachment",
  "link" : "http://s-10.server.host.com/f/2015/01/01/a/file.json",
  "mime_type" : "application/json",
  "name" : "file.json",
  "owner_user_id" : "21312411",
  "owner_user_name" : "Nicky McCartney",
  "parent_id" : "21312411",
  "parent_name" : "Bill Wall",
  "parent_type" : "contact",
  "size" : 34345,
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createAttachmentEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/attachment',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all attachments from the system
 * @method
 * @name Data2CRMAPI#getAttachmentCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAttachmentCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/attachment/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for attachments
 * @method
 * @name Data2CRMAPI#getAttachmentDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "attachment",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAttachmentDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/attachment/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return attachment information
 * @method
 * @name Data2CRMAPI#getAttachmentEntity
 * @param {string} attachment_id - REQUIRED - Attachment Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getAttachmentEntity = function(attachment_id, config, callback) {
  restletUtils.checkPathVariables(attachment_id, 'attachment_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/attachment/' + attachment_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update attachment information
 * @method
 * @name Data2CRMAPI#updateAttachmentEntity
 * @param {string} attachment_id - REQUIRED - Attachment Identifier
 * @param {object} body - Update attachment information; is of type: AttachmentUpdate; has the following structure:
{
  "contact_name" : "Bill Wall",
  "created_at" : null,
  "description" : "Description of attachment",
  "link" : "http://s-10.server.host.com/f/2015/01/01/a/file.json",
  "mime_type" : "application/json",
  "name" : "file.json",
  "owner_user_id" : "21312411",
  "owner_user_name" : "Nicky McCartney",
  "parent_id" : "21312411",
  "parent_name" : "Bill Wall",
  "parent_type" : "contact",
  "size" : 34345,
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateAttachmentEntity = function(attachment_id, body, config, callback) {
  restletUtils.checkPathVariables(attachment_id, 'attachment_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/attachment/' + attachment_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete attachment information
 * @method
 * @name Data2CRMAPI#deleteAttachmentEntity
 * @param {string} attachment_id - REQUIRED - Attachment Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteAttachmentEntity = function(attachment_id, config, callback) {
  restletUtils.checkPathVariables(attachment_id, 'attachment_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/attachment/' + attachment_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all calls from the system
 * @method
 * @name Data2CRMAPI#getCallCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.parent_type - Parent Type
 * @param {string} config.queryParameters.parent_id - Parent Identifier
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCallCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/call',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add call into the system
 * @method
 * @name Data2CRMAPI#createCallEntity
 * @param {object} body - Add call into the system; is of type: CallCreate; has the following structure:
{
  "contact_id" : "21312411",
  "created_at" : null,
  "description" : "Description of the call",
  "direction" : "Outbound",
  "ended_at" : null,
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "purpose" : "Negotiation",
  "result" : "Confirmed",
  "started_at" : null,
  "status" : "Planned",
  "subject" : "Subject of the call",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createCallEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/call',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all calls from the system
 * @method
 * @name Data2CRMAPI#getCallCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCallCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/call/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for calls
 * @method
 * @name Data2CRMAPI#getCallDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "call",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCallDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/call/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return call information
 * @method
 * @name Data2CRMAPI#getCallEntity
 * @param {string} call_id - REQUIRED - Call Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCallEntity = function(call_id, config, callback) {
  restletUtils.checkPathVariables(call_id, 'call_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/call/' + call_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update call information
 * @method
 * @name Data2CRMAPI#updateCallEntity
 * @param {string} call_id - REQUIRED - Call Identifier
 * @param {object} body - Update call information; is of type: CallUpdate; has the following structure:
{
  "contact_id" : "21312411",
  "created_at" : null,
  "description" : "Description of the call",
  "direction" : "Outbound",
  "ended_at" : null,
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "purpose" : "Negotiation",
  "result" : "Confirmed",
  "started_at" : null,
  "status" : "Planned",
  "subject" : "Subject of the call",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateCallEntity = function(call_id, body, config, callback) {
  restletUtils.checkPathVariables(call_id, 'call_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/call/' + call_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete call information
 * @method
 * @name Data2CRMAPI#deleteCallEntity
 * @param {string} call_id - REQUIRED - Call Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteCallEntity = function(call_id, config, callback) {
  restletUtils.checkPathVariables(call_id, 'call_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/call/' + call_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all contacts from the system
 * @method
 * @name Data2CRMAPI#getContactCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.name - Name
 * @param {string} config.queryParameters.first_name - First Name
 * @param {string} config.queryParameters.last_name - Last Name
 * @param {string} config.queryParameters.email - Email
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getContactCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/contact',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add contact into the system
 * @method
 * @name Data2CRMAPI#createContactEntity
 * @param {object} body - Add contact into the system; is of type: ContactCreate; has the following structure:
{
  "account_id" : "21312411",
  "address_city" : "Brandenburg",
  "address_country" : "USA",
  "address_state" : "NY",
  "address_street" : "P.O. Box 300, 2698 Ultricies Road",
  "address_zip" : "47212",
  "alternate_address_city" : "Brandenburg",
  "alternate_address_country" : "USA",
  "alternate_address_state" : "NY",
  "alternate_address_street" : "P.O. Box 300, 2698 Ultricies Road",
  "alternate_address_zip" : "47212",
  "date_created" : null,
  "date_updated" : null,
  "department" : "D2C",
  "description" : "Description of the contact",
  "do_not_call" : false,
  "email" : "bill.wall@mail.com",
  "email_alt" : "bill.wall.alt@mail.com",
  "fax" : "+49 9131 85 28732",
  "first_name" : "Bill",
  "full_name" : "Bill Wall",
  "last_name" : "Wall",
  "lead_source" : "Partner",
  "name" : "Bill Wall",
  "owner_id" : "21312411",
  "phone_home" : "(817) 569-8900",
  "phone_mobile" : "(817) 569-8900",
  "phone_work" : "(817) 569-8900",
  "report_to_id" : "21312411",
  "salutation" : "Mr.",
  "sync_to_outlook" : false,
  "title" : "Title",
  "type" : "Bill Wall",
  "user" : "Bill Wall",
  "user_id" : "21312411"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createContactEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/contact',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all contacts from the system
 * @method
 * @name Data2CRMAPI#getContactCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getContactCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/contact/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for contacts
 * @method
 * @name Data2CRMAPI#getContactDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "contact",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getContactDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/contact/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return contact information
 * @method
 * @name Data2CRMAPI#getContactEntity
 * @param {string} contact_id - REQUIRED - Contact Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getContactEntity = function(contact_id, config, callback) {
  restletUtils.checkPathVariables(contact_id, 'contact_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/contact/' + contact_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update contact information
 * @method
 * @name Data2CRMAPI#updateContactEntity
 * @param {string} contact_id - REQUIRED - Contact Identifier
 * @param {object} body - Update contact information; is of type: ContactUpdate; has the following structure:
{
  "account_id" : "21312411",
  "address_city" : "Brandenburg",
  "address_country" : "USA",
  "address_state" : "NY",
  "address_street" : "P.O. Box 300, 2698 Ultricies Road",
  "address_zip" : "47212",
  "alternate_address_city" : "Brandenburg",
  "alternate_address_country" : "USA",
  "alternate_address_state" : "NY",
  "alternate_address_street" : "P.O. Box 300, 2698 Ultricies Road",
  "alternate_address_zip" : "47212",
  "date_created" : null,
  "date_updated" : null,
  "department" : "D2C",
  "description" : "Description of the contact",
  "do_not_call" : false,
  "email" : "bill.wall@mail.com",
  "email_alt" : "bill.wall.alt@mail.com",
  "fax" : "+49 9131 85 28732",
  "first_name" : "Bill",
  "full_name" : "Bill Wall",
  "last_name" : "Wall",
  "lead_source" : "Partner",
  "name" : "Bill Wall",
  "owner_id" : "21312411",
  "phone_home" : "(817) 569-8900",
  "phone_mobile" : "(817) 569-8900",
  "phone_work" : "(817) 569-8900",
  "report_to_id" : "21312411",
  "salutation" : "Mr.",
  "sync_to_outlook" : false,
  "title" : "Title",
  "type" : "Bill Wall",
  "user" : "Bill Wall",
  "user_id" : "21312411"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateContactEntity = function(contact_id, body, config, callback) {
  restletUtils.checkPathVariables(contact_id, 'contact_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/contact/' + contact_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete contact information
 * @method
 * @name Data2CRMAPI#deleteContactEntity
 * @param {string} contact_id - REQUIRED - Contact Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteContactEntity = function(contact_id, config, callback) {
  restletUtils.checkPathVariables(contact_id, 'contact_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/contact/' + contact_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all CRMs from the system
 * @method
 * @name Data2CRMAPI#getCrmCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.type - Type
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "api_url" : "https://crm.com/api/v2.1/",
  "id" : "b716431423997ba4d35a2a81a10dc6b5167ffb53",
  "type" : "Salesforce"
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCrmCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/crm',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add CRM into the system<br><a href="http://www.data2crm.com/api/faqs/need-connect-crm-data2crm-api/" target="_blank">What do I need to connect a CRM to Data2CRM.API?</a>
 * @method
 * @name Data2CRMAPI#createCrmEntity
 * @param {object} body - Add CRM into the system<br><a href="http://www.data2crm.com/api/faqs/need-connect-crm-data2crm-api/" target="_blank">What do I need to connect a CRM to Data2CRM.API?</a>; is of type: CrmCreate; has the following structure:
{
  "api_url" : "https://crm.com/api/v2.1/",
  "login" : "admin",
  "password" : "bg64d4245rgd233",
  "type" : "Salesforce"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "b716431423997ba4d35a2a81a10dc6b5167ffb53"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createCrmEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/crm',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all CRMs from the system
 * @method
 * @name Data2CRMAPI#getCrmCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCrmCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/crm/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return CRM information
 * @method
 * @name Data2CRMAPI#getCrmEntity
 * @param {string} crm_id - REQUIRED - CRM Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "api_url" : "https://crm.com/api/v2.1/",
  "id" : "b716431423997ba4d35a2a81a10dc6b5167ffb53",
  "type" : "Salesforce"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getCrmEntity = function(crm_id, config, callback) {
  restletUtils.checkPathVariables(crm_id, 'crm_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/crm/' + crm_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update CRM information<br><a href="http://www.data2crm.com/api/faqs/need-connect-crm-data2crm-api/" target="_blank">What do I need to connect a CRM to Data2CRM.API?</a>
 * @method
 * @name Data2CRMAPI#updateCrmEntity
 * @param {string} crm_id - REQUIRED - CRM Identifier
 * @param {object} body - Update CRM information<br><a href="http://www.data2crm.com/api/faqs/need-connect-crm-data2crm-api/" target="_blank">What do I need to connect a CRM to Data2CRM.API?</a>; is of type: CrmUpdate; has the following structure:
{
  "api_url" : "https://crm.com/api/v2.1/",
  "login" : "admin",
  "password" : "bg64d4245rgd233",
  "type" : "Salesforce"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "b716431423997ba4d35a2a81a10dc6b5167ffb53"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateCrmEntity = function(crm_id, body, config, callback) {
  restletUtils.checkPathVariables(crm_id, 'crm_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/crm/' + crm_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete CRM information
 * @method
 * @name Data2CRMAPI#deleteCrmEntity
 * @param {string} crm_id - REQUIRED - CRM Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteCrmEntity = function(crm_id, config, callback) {
  restletUtils.checkPathVariables(crm_id, 'crm_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/crm/' + crm_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all emails from the system
 * @method
 * @name Data2CRMAPI#getEmailCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.parent_type - Parent Type
 * @param {string} config.queryParameters.parent_id - Parent Identifier
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEmailCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/email',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add email into the system
 * @method
 * @name Data2CRMAPI#createEmailEntity
 * @param {object} body - Add email into the system; is of type: EmailCreate; has the following structure:
{
  "bcc" : "bill.wall.alt@mail.com",
  "body" : "Body of the email",
  "cc" : "bill.wall.alt@mail.com",
  "created_at" : null,
  "direction" : "Outbound",
  "from" : "bill.wall@mail.com",
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "sent_at" : null,
  "status" : "Sent",
  "subject" : "Subject of the email",
  "to" : "bill.wall.alt@mail.com",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createEmailEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/email',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all emails from the system
 * @method
 * @name Data2CRMAPI#getEmailCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEmailCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/email/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for emails
 * @method
 * @name Data2CRMAPI#getEmailDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "email",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEmailDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/email/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return email information
 * @method
 * @name Data2CRMAPI#getEmailEntity
 * @param {string} email_id - REQUIRED - Email Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEmailEntity = function(email_id, config, callback) {
  restletUtils.checkPathVariables(email_id, 'email_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/email/' + email_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update email information
 * @method
 * @name Data2CRMAPI#updateEmailEntity
 * @param {string} email_id - REQUIRED - Email Identifier
 * @param {object} body - Update email information; is of type: EmailUpdate; has the following structure:
{
  "bcc" : "bill.wall.alt@mail.com",
  "body" : "Body of the email",
  "cc" : "bill.wall.alt@mail.com",
  "created_at" : null,
  "direction" : "Outbound",
  "from" : "bill.wall@mail.com",
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "sent_at" : null,
  "status" : "Sent",
  "subject" : "Subject of the email",
  "to" : "bill.wall.alt@mail.com",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateEmailEntity = function(email_id, body, config, callback) {
  restletUtils.checkPathVariables(email_id, 'email_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/email/' + email_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete email information
 * @method
 * @name Data2CRMAPI#deleteEmailEntity
 * @param {string} email_id - REQUIRED - Email Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteEmailEntity = function(email_id, config, callback) {
  restletUtils.checkPathVariables(email_id, 'email_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/email/' + email_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all events from the system
 * @method
 * @name Data2CRMAPI#getEventCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.parent_type - Parent Type
 * @param {string} config.queryParameters.parent_id - Parent Identifier
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEventCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/event',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add event into the system
 * @method
 * @name Data2CRMAPI#createEventEntity
 * @param {object} body - Add event into the system; is of type: EventCreate; has the following structure:
{
  "access" : "Public",
  "contact_id" : "21312411",
  "created_at" : null,
  "description" : "Description of the event",
  "ended_at" : null,
  "is_all_day" : false,
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "started_at" : null,
  "subject" : "Subject of the event",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createEventEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/event',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all events from the system
 * @method
 * @name Data2CRMAPI#getEventCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEventCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/event/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for events
 * @method
 * @name Data2CRMAPI#getEventDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "event",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEventDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/event/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return event information
 * @method
 * @name Data2CRMAPI#getEventEntity
 * @param {string} event_id - REQUIRED - Event Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getEventEntity = function(event_id, config, callback) {
  restletUtils.checkPathVariables(event_id, 'event_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/event/' + event_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update event information
 * @method
 * @name Data2CRMAPI#updateEventEntity
 * @param {string} event_id - REQUIRED - Event Identifier
 * @param {object} body - Update event information; is of type: EventUpdate; has the following structure:
{
  "access" : "Public",
  "contact_id" : "21312411",
  "created_at" : null,
  "description" : "Description of the event",
  "ended_at" : null,
  "is_all_day" : false,
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "started_at" : null,
  "subject" : "Subject of the event",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateEventEntity = function(event_id, body, config, callback) {
  restletUtils.checkPathVariables(event_id, 'event_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/event/' + event_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete event information
 * @method
 * @name Data2CRMAPI#deleteEventEntity
 * @param {string} event_id - REQUIRED - Event Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteEventEntity = function(event_id, config, callback) {
  restletUtils.checkPathVariables(event_id, 'event_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/event/' + event_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all leads from the system
 * @method
 * @name Data2CRMAPI#getLeadCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.first_name - First Name
 * @param {string} config.queryParameters.last_name - Last Name
 * @param {string} config.queryParameters.email - Email
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getLeadCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/lead',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add lead into the system
 * @method
 * @name Data2CRMAPI#createLeadEntity
 * @param {object} body - Add lead into the system; is of type: LeadCreate; has the following structure:
{
  "account_id" : "21312411",
  "account_name" : "Bill Wall",
  "alt_city" : "Brandenburg",
  "alt_country" : "USA",
  "alt_state" : "NY",
  "alt_street" : "P.O. Box 300, 2698 Ultricies Road",
  "alt_zip_code" : "47212",
  "annual_revenue" : 100000.0,
  "birth_date" : null,
  "campaign_id" : "21312411",
  "city" : "Brandenburg",
  "company" : "M&M",
  "contact_id" : "21312411",
  "country" : "USA",
  "date_created" : null,
  "date_updated" : null,
  "department" : "D2C",
  "description" : "Description",
  "do_not_call" : false,
  "email" : "bill.wall@mail.com",
  "email_home" : "bill.wall@mail.com",
  "email_other" : "bill.wall.alt@mail.com",
  "event_id" : "21312411",
  "fax" : "+49 9131 85 28732",
  "first_name" : "Bill",
  "industry" : "Construction",
  "last_name" : "Wall",
  "lead_source" : "sample lead_source",
  "lead_source_description" : "Website Application",
  "opened" : "sample opened",
  "opportunity_amount" : 442.21,
  "opportunity_id" : "21312411",
  "owner_id" : "21312411",
  "pager" : "123-12",
  "phone_home" : "(817) 569-8900",
  "phone_mobile" : "(817) 569-8900",
  "phone_other" : "(817) 569-8900",
  "phone_work" : "(817) 569-8900",
  "referred_by" : "Bill Wall",
  "salutation" : "Mr.",
  "skype_id" : "bill.wall",
  "state" : "NY",
  "status" : "Unassigned",
  "status_description" : "Description",
  "street" : "P.O. Box 300, 2698 Ultricies Road",
  "suite" : "sample suite",
  "task_id" : "21312411",
  "title" : "Title",
  "type" : "Bill Wall",
  "website" : "http://google.com/",
  "zip_code" : "47212"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createLeadEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/lead',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all leads from the system
 * @method
 * @name Data2CRMAPI#getLeadCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getLeadCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/lead/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for leads
 * @method
 * @name Data2CRMAPI#getLeadDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "lead",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getLeadDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/lead/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return lead information
 * @method
 * @name Data2CRMAPI#getLeadEntity
 * @param {string} lead_id - REQUIRED - Lead Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getLeadEntity = function(lead_id, config, callback) {
  restletUtils.checkPathVariables(lead_id, 'lead_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/lead/' + lead_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update lead information
 * @method
 * @name Data2CRMAPI#updateLeadEntity
 * @param {string} lead_id - REQUIRED - Lead Identifier
 * @param {object} body - Update lead information; is of type: LeadUpdate; has the following structure:
{
  "account_id" : "21312411",
  "account_name" : "Bill Wall",
  "alt_city" : "Brandenburg",
  "alt_country" : "USA",
  "alt_state" : "NY",
  "alt_street" : "P.O. Box 300, 2698 Ultricies Road",
  "alt_zip_code" : "47212",
  "annual_revenue" : 100000.0,
  "birth_date" : null,
  "campaign_id" : "21312411",
  "city" : "Brandenburg",
  "company" : "M&M",
  "contact_id" : "21312411",
  "country" : "USA",
  "date_created" : null,
  "date_updated" : null,
  "department" : "D2C",
  "description" : "Description",
  "do_not_call" : false,
  "email" : "bill.wall@mail.com",
  "email_home" : "bill.wall@mail.com",
  "email_other" : "bill.wall.alt@mail.com",
  "event_id" : "21312411",
  "fax" : "+49 9131 85 28732",
  "first_name" : "Bill",
  "industry" : "Construction",
  "last_name" : "Wall",
  "lead_source" : "sample lead_source",
  "lead_source_description" : "Website Application",
  "opened" : "sample opened",
  "opportunity_amount" : 442.21,
  "opportunity_id" : "21312411",
  "owner_id" : "21312411",
  "pager" : "123-12",
  "phone_home" : "(817) 569-8900",
  "phone_mobile" : "(817) 569-8900",
  "phone_other" : "(817) 569-8900",
  "phone_work" : "(817) 569-8900",
  "referred_by" : "Bill Wall",
  "salutation" : "Mr.",
  "skype_id" : "bill.wall",
  "state" : "NY",
  "status" : "Unassigned",
  "status_description" : "Description",
  "street" : "P.O. Box 300, 2698 Ultricies Road",
  "suite" : "sample suite",
  "task_id" : "21312411",
  "title" : "Title",
  "type" : "Bill Wall",
  "website" : "http://google.com/",
  "zip_code" : "47212"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateLeadEntity = function(lead_id, body, config, callback) {
  restletUtils.checkPathVariables(lead_id, 'lead_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/lead/' + lead_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete lead information
 * @method
 * @name Data2CRMAPI#deleteLeadEntity
 * @param {string} lead_id - REQUIRED - Lead Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteLeadEntity = function(lead_id, config, callback) {
  restletUtils.checkPathVariables(lead_id, 'lead_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/lead/' + lead_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all meetings from the system
 * @method
 * @name Data2CRMAPI#getMeetingCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.parent_type - Parent Type
 * @param {string} config.queryParameters.parent_id - Parent Identifier
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getMeetingCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/meeting',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add meeting into the system
 * @method
 * @name Data2CRMAPI#createMeetingEntity
 * @param {object} body - Add meeting into the system; is of type: MeetingCreate; has the following structure:
{
  "contact_id" : "21312411",
  "created_at" : null,
  "description" : "Description of the meeting",
  "ended_at" : null,
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "result" : "Confirmed",
  "started_at" : null,
  "status" : "Held",
  "subject" : "My first note",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createMeetingEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/meeting',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all meetings from the system
 * @method
 * @name Data2CRMAPI#getMeetingCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getMeetingCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/meeting/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for meetings
 * @method
 * @name Data2CRMAPI#getMeetingDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "meeting",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getMeetingDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/meeting/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return meeting information
 * @method
 * @name Data2CRMAPI#getMeetingEntity
 * @param {string} meeting_id - REQUIRED - Meeting Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getMeetingEntity = function(meeting_id, config, callback) {
  restletUtils.checkPathVariables(meeting_id, 'meeting_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/meeting/' + meeting_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update meeting information
 * @method
 * @name Data2CRMAPI#updateMeetingEntity
 * @param {string} meeting_id - REQUIRED - Meeting Identifier
 * @param {object} body - Update meeting information; is of type: MeetingUpdate; has the following structure:
{
  "contact_id" : "21312411",
  "created_at" : null,
  "description" : "Description of the meeting",
  "ended_at" : null,
  "owner_user_id" : "21312411",
  "parent_id" : "21312411",
  "parent_type" : "contact",
  "result" : "Confirmed",
  "started_at" : null,
  "status" : "Held",
  "subject" : "My first note",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateMeetingEntity = function(meeting_id, body, config, callback) {
  restletUtils.checkPathVariables(meeting_id, 'meeting_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/meeting/' + meeting_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete meeting information
 * @method
 * @name Data2CRMAPI#deleteMeetingEntity
 * @param {string} meeting_id - REQUIRED - Meeting Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteMeetingEntity = function(meeting_id, config, callback) {
  restletUtils.checkPathVariables(meeting_id, 'meeting_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/meeting/' + meeting_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all notes from the system
 * @method
 * @name Data2CRMAPI#getNoteCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {string} config.queryParameters.parent_type - Parent Type
 * @param {string} config.queryParameters.parent_id - Parent Identifier
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getNoteCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/note',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add note into the system
 * @method
 * @name Data2CRMAPI#createNoteEntity
 * @param {object} body - Add note into the system; is of type: NoteCreate; has the following structure:
{
  "body" : "Body of the note",
  "contact_name" : "Bill Wall",
  "created_at" : null,
  "owner_user_id" : "21312411",
  "owner_user_name" : "Nicky McCartney",
  "parent_id" : "21312411",
  "parent_name" : "Bill Wall",
  "parent_type" : "contact",
  "subject" : "My first note",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createNoteEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/note',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all notes from the system
 * @method
 * @name Data2CRMAPI#getNoteCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getNoteCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/note/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for notes
 * @method
 * @name Data2CRMAPI#getNoteDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "note",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getNoteDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/note/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return note information
 * @method
 * @name Data2CRMAPI#getNoteEntity
 * @param {string} note_id - REQUIRED - Note Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getNoteEntity = function(note_id, config, callback) {
  restletUtils.checkPathVariables(note_id, 'note_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/note/' + note_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update note information
 * @method
 * @name Data2CRMAPI#updateNoteEntity
 * @param {string} note_id - REQUIRED - Note Identifier
 * @param {object} body - Update note information; is of type: NoteUpdate; has the following structure:
{
  "body" : "Body of the note",
  "contact_name" : "Bill Wall",
  "created_at" : null,
  "owner_user_id" : "21312411",
  "owner_user_name" : "Nicky McCartney",
  "parent_id" : "21312411",
  "parent_name" : "Bill Wall",
  "parent_type" : "contact",
  "subject" : "My first note",
  "updated_at" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateNoteEntity = function(note_id, body, config, callback) {
  restletUtils.checkPathVariables(note_id, 'note_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/note/' + note_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete note information
 * @method
 * @name Data2CRMAPI#deleteNoteEntity
 * @param {string} note_id - REQUIRED - Note Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteNoteEntity = function(note_id, config, callback) {
  restletUtils.checkPathVariables(note_id, 'note_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/note/' + note_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all opportunities from the system
 * @method
 * @name Data2CRMAPI#getOpportunityCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getOpportunityCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/opportunity',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add opportunity into the system
 * @method
 * @name Data2CRMAPI#createOpportunityEntity
 * @param {object} body - Add opportunity into the system; is of type: OpportunityCreate; has the following structure:
{
  "account_id" : "21312411",
  "account_name" : "Bill Wall",
  "amount" : 4235.9,
  "assigned_user_id" : "21312411",
  "assigned_user_name" : "Bill Wall",
  "campaign_id" : "21312411",
  "campaign_name" : "D2C",
  "contact_id" : "21312411",
  "currency_name" : "US Dollar",
  "currency_symbol" : "USD",
  "date_closed" : null,
  "date_created" : null,
  "description" : "Description",
  "lead_id" : "21312411",
  "lead_source" : "Web Site",
  "name" : "Bill Wall",
  "next_step" : "Some",
  "owner_id" : "21312411",
  "probability" : 80.0,
  "sales_stage" : "Negotiation",
  "type" : "Sales"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createOpportunityEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/opportunity',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all opportunities from the system
 * @method
 * @name Data2CRMAPI#getOpportunityCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getOpportunityCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/opportunity/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for opportunities
 * @method
 * @name Data2CRMAPI#getOpportunityDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "opportunity",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getOpportunityDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/opportunity/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return opportunity information
 * @method
 * @name Data2CRMAPI#getOpportunityEntity
 * @param {string} opportunity_id - REQUIRED - Opportunity Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getOpportunityEntity = function(opportunity_id, config, callback) {
  restletUtils.checkPathVariables(opportunity_id, 'opportunity_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/opportunity/' + opportunity_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update opportunity information
 * @method
 * @name Data2CRMAPI#updateOpportunityEntity
 * @param {string} opportunity_id - REQUIRED - Opportunity Identifier
 * @param {object} body - Update opportunity information; is of type: OpportunityUpdate; has the following structure:
{
  "account_id" : "21312411",
  "account_name" : "Bill Wall",
  "amount" : 4235.9,
  "assigned_user_id" : "21312411",
  "assigned_user_name" : "Bill Wall",
  "campaign_id" : "21312411",
  "campaign_name" : "D2C",
  "contact_id" : "21312411",
  "currency_name" : "US Dollar",
  "currency_symbol" : "USD",
  "date_closed" : null,
  "date_created" : null,
  "description" : "Description",
  "lead_id" : "21312411",
  "lead_source" : "Web Site",
  "name" : "Bill Wall",
  "next_step" : "Some",
  "owner_id" : "21312411",
  "probability" : 80.0,
  "sales_stage" : "Negotiation",
  "type" : "Sales"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateOpportunityEntity = function(opportunity_id, body, config, callback) {
  restletUtils.checkPathVariables(opportunity_id, 'opportunity_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/opportunity/' + opportunity_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete opportunity information
 * @method
 * @name Data2CRMAPI#deleteOpportunityEntity
 * @param {string} opportunity_id - REQUIRED - Opportunity Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteOpportunityEntity = function(opportunity_id, config, callback) {
  restletUtils.checkPathVariables(opportunity_id, 'opportunity_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/opportunity/' + opportunity_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all tasks from the system
 * @method
 * @name Data2CRMAPI#getTaskCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getTaskCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/task',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add task into the system
 * @method
 * @name Data2CRMAPI#createTaskEntity
 * @param {object} body - Add task into the system; is of type: TaskCreate; has the following structure:
{
  "account_id" : "21312411",
  "assigned_user_id" : "21312411",
  "contact" : "Bill Wall",
  "contact_id" : "21312411",
  "created_date" : null,
  "description" : "First task",
  "due_date" : null,
  "lead_id" : "21312411",
  "name" : "Title",
  "opportunity" : "Title",
  "opportunity_id" : "21312411",
  "owner" : "Bill Wall",
  "owner_id" : "21312411",
  "parent_id" : "21312411",
  "priority" : true,
  "publicly_visible" : true,
  "reminder_date" : null,
  "reporter_id" : "21312411",
  "start_date" : null,
  "status" : "In Progress",
  "updated_date" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createTaskEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/task',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all tasks from the system
 * @method
 * @name Data2CRMAPI#getTaskCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getTaskCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/task/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for tasks
 * @method
 * @name Data2CRMAPI#getTaskDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "task",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getTaskDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/task/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return task information
 * @method
 * @name Data2CRMAPI#getTaskEntity
 * @param {string} task_id - REQUIRED - Task Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getTaskEntity = function(task_id, config, callback) {
  restletUtils.checkPathVariables(task_id, 'task_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/task/' + task_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update task information
 * @method
 * @name Data2CRMAPI#updateTaskEntity
 * @param {string} task_id - REQUIRED - Task Identifier
 * @param {object} body - Update task information; is of type: TaskUpdate; has the following structure:
{
  "account_id" : "21312411",
  "assigned_user_id" : "21312411",
  "contact" : "Bill Wall",
  "contact_id" : "21312411",
  "created_date" : null,
  "description" : "First task",
  "due_date" : null,
  "lead_id" : "21312411",
  "name" : "Title",
  "opportunity" : "Title",
  "opportunity_id" : "21312411",
  "owner" : "Bill Wall",
  "owner_id" : "21312411",
  "parent_id" : "21312411",
  "priority" : true,
  "publicly_visible" : true,
  "reminder_date" : null,
  "reporter_id" : "21312411",
  "start_date" : null,
  "status" : "In Progress",
  "updated_date" : null
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateTaskEntity = function(task_id, body, config, callback) {
  restletUtils.checkPathVariables(task_id, 'task_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/task/' + task_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete task information
 * @method
 * @name Data2CRMAPI#deleteTaskEntity
 * @param {string} task_id - REQUIRED - Task Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteTaskEntity = function(task_id, config, callback) {
  restletUtils.checkPathVariables(task_id, 'task_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/task/' + task_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns all users from the system
 * @method
 * @name Data2CRMAPI#getUserCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {integer} config.queryParameters.limit - Amount of results (default: 25)
 * @param {integer} config.queryParameters.offset - Start from record (default: 0)
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
[{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}]
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getUserCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/user',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Add user into the system
 * @method
 * @name Data2CRMAPI#createUserEntity
 * @param {object} body - Add user into the system; is of type: UserCreate; has the following structure:
{
  "address_city" : "Brandenburg",
  "address_country" : "USA",
  "address_postal_code" : "47212",
  "address_state" : "NY",
  "address_street" : "P.O. Box 300, 2698 Ultricies Road",
  "confirm_password" : "bv64c566b756df3",
  "created_by" : "21312411",
  "department" : "Department",
  "description" : "Description",
  "email" : "bill.wall@mail.com",
  "email_other" : "bill.wall@mail.com",
  "firstname" : "Bill",
  "fullname" : "Bill Wall",
  "is_active" : true,
  "is_admin" : true,
  "lastname" : "Wall",
  "modified_by" : "21312411",
  "name" : "Title",
  "phone_home" : "(817) 569-8900",
  "phone_mobile" : "(817) 569-8900",
  "phone_other" : "(817) 569-8900",
  "phone_work" : "(817) 569-8900",
  "position" : "Position",
  "role" : "CEO",
  "roleid" : "21312411",
  "signature" : "Signature",
  "status" : "active",
  "title" : "Title",
  "user_password" : "bv64c566b756df3",
  "website" : "http://google.com/"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 201 - Created - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.createUserEntity = function(body, config, callback) {
  restletUtils.executeRequest.call(this, 'POST',
    this.endpoint + '/user',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Count all users from the system
 * @method
 * @name Data2CRMAPI#getUserCountCollection
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "total" : 7612
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getUserCountCollection = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/user/count',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Returns describe for users
 * @method
 * @name Data2CRMAPI#getUserDescribe
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "entity" : "user",
  "schema" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getUserDescribe = function(config, callback) {
  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/user/describe',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Return user information
 * @method
 * @name Data2CRMAPI#getUserEntity
 * @param {string} user_id - REQUIRED - User Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "embedded" : null,
  "id" : "21312411",
  "relation" : null
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.getUserEntity = function(user_id, config, callback) {
  restletUtils.checkPathVariables(user_id, 'user_id');

  restletUtils.executeRequest.call(this, 'GET',
    this.endpoint + '/user/' + user_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

/**
 * Update user information
 * @method
 * @name Data2CRMAPI#updateUserEntity
 * @param {string} user_id - REQUIRED - User Identifier
 * @param {object} body - Update user information; is of type: UserUpdate; has the following structure:
{
  "address_city" : "Brandenburg",
  "address_country" : "USA",
  "address_postal_code" : "47212",
  "address_state" : "NY",
  "address_street" : "P.O. Box 300, 2698 Ultricies Road",
  "confirm_password" : "bv64c566b756df3",
  "created_by" : "21312411",
  "department" : "Department",
  "description" : "Description",
  "email" : "bill.wall@mail.com",
  "email_other" : "bill.wall@mail.com",
  "firstname" : "Bill",
  "fullname" : "Bill Wall",
  "is_active" : true,
  "is_admin" : true,
  "lastname" : "Wall",
  "modified_by" : "21312411",
  "name" : "Title",
  "phone_home" : "(817) 569-8900",
  "phone_mobile" : "(817) 569-8900",
  "phone_other" : "(817) 569-8900",
  "phone_work" : "(817) 569-8900",
  "position" : "Position",
  "role" : "CEO",
  "roleid" : "21312411",
  "signature" : "Signature",
  "status" : "active",
  "title" : "Title",
  "user_password" : "bv64c566b756df3",
  "website" : "http://google.com/"
}
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *    - Status code : 200 - OK - Payload :
{
  "id" : "21312411"
}
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.updateUserEntity = function(user_id, body, config, callback) {
  restletUtils.checkPathVariables(user_id, 'user_id');

  restletUtils.executeRequest.call(this, 'PUT',
    this.endpoint + '/user/' + user_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations),
    body
  );
};

/**
 * Delete user information
 * @method
 * @name Data2CRMAPI#deleteUserEntity
 * @param {string} user_id - REQUIRED - User Identifier
 * @param {object} config - the configuration object containing the query parameters and additional headers.
 * @param {object} config.headers - headers to use for the request in addition to the default ones.
 * @param {object} config.queryParameters - query parameters to use for the request in addition to the default ones.
 * @param {Function} callback - the callback called after request completion with the following parameters:
 *  - error if any technical error occured or if the response's status does not belong to the 2xx range. In that case the error would have the following structure:
{
  status : 400,
  message : 'The request cannot be fulfilled due to XXX'
}
 *  - body of the response auto-extracted from the response if the status is in the 2xx range.
 *  - response the technical (low-level) node response (c.f. https://nodejs.org/api/http.html#http_http_incomingmessage)
 */
Data2CRMAPI.prototype.deleteUserEntity = function(user_id, config, callback) {
  restletUtils.checkPathVariables(user_id, 'user_id');

  restletUtils.executeRequest.call(this, 'DELETE',
    this.endpoint + '/user/' + user_id + '',
    callback,
    securityUtils.addSecurityConfiguration(config, this.globalSecurity, this.securityConfigurations)
  );
};

module.exports = Data2CRMAPI;
