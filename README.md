[All SDKs](/sdk)

# Node.js SDK

   * [Installation](#installation)
   * [Getting Started](#getting-started)
   * [How to Use](#how-to-use)
       * [Describe a resource](#describe-a-resource)
       * [Count all of resources](#count-all-of-resources)
       * [Fetch all of resources](#fetch-all-of-resources)
       * [Fetch a resource](#fetch-a-resource)
       * [Create a resource](#create-a-resource)
       * [Update a resource](#update-a-resource)
       * [Delete a resource](#delete-a-resource)

## <a name="installation"></a>Installation

Install with the Node.JS package manager npm

    npm install data2crm-api-nodejs-sdk

Alternately, manually add the following to your `package.json`, in the `dependencies` section:

    "dependencies": {
        "data2crm-api-nodejs-sdk": "1.0.3"
    }

## <a name="getting-started"></a>Getting Started

    // Require Data2CRMAPI SDK
    var Data2CRMAPISDK = require('data2crm-api-nodejs-sdk');

    // Create Data2CRMAPI
    var Data2CRMAPI = new Data2CRMAPISDK.Data2CRMAPI();

    // Set user key
    Data2CRMAPI.configureGlobalApiToken('X-API2CRM-USERKEY', '<USER_KEY>', 'header'); // change <USER_KEY> to your user key

## <a name="how-to-use"></a>How to Use

### <a name="describe-a-resource"></a>Describe a resource

    Data2CRMAPI.getContactDescribe(
        {
          headers: {
            'Accept': 'application/json',
            'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
          }
        },
        function(err, describe, response) {
          if (err) {
            if (err.status >= 400 && err.status <= 499) {
              console.log('Client error ' + err.status + ': ', err.message);
            } else if (err.status >= 500 && err.status <= 599) {
              console.log('Server error ' + err.status + ': ', err.message);
            } else {
              console.log('Unknown error ' + err.status + ': ', err.message);
            }
          } else {
            console.log('Describe contact result: ', describe);
          }
        }
    );

### <a name="count-all-of-resources"></a>Count all of resources

    Data2CRMAPI.getContactCountCollection(
        {
            headers: {
                'Accept': 'application/json',
                'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
            }
        },
        function(err, count, response) {
            if (err) {
                if (err.status >= 400 && err.status <= 499) {
                    console.log('Client error ' + err.status + ': ', err.message);
                } else if (err.status >= 500 && err.status <= 599) {
                    console.log('Server error ' + err.status + ': ', err.message);
                } else {
                    console.log('Unknown error ' + err.status + ': ', err.message);
                }
            } else {
                console.log('Count contact collection result: ', count);
            }
        }
    );

### <a name="fetch-all-of-resources"></a>Fetch all of resources

    Data2CRMAPI.getContactCollection(
        {
            headers: {
                'Accept': 'application/json',
                'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
            }
        },
        function(err, contact, response) {
            if (err) {
                if (err.status >= 400 && err.status <= 499) {
                    console.log('Client error ' + err.status + ': ', err.message);
                } else if (err.status >= 500 && err.status <= 599) {
                    console.log('Server error ' + err.status + ': ', err.message);
                } else {
                    console.log('Unknown error ' + err.status + ': ', err.message);
                }
            } else {
                console.log('Contact collection result: ', contact);
            }
        }
    );

### <a name="fetch-a-resource"></a>Fetch a resource

    Data2CRMAPI.getContactEntity(
        '003580000024E1IAAU',
        {
            headers: {
                'Accept': 'application/json',
                'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
            }
        },
        function(err, contact, response) {
            if (err) {
                if (err.status >= 400 && err.status <= 499) {
                    console.log('Client error ' + err.status + ': ', err.message);
                } else if (err.status >= 500 && err.status <= 599) {
                    console.log('Server error ' + err.status + ': ', err.message);
                } else {
                    console.log('Unknown error ' + err.status + ': ', err.message);
                }
            } else {
                console.log('Contact "003580000024E1IAAU" result:', contact);
            }
        }
    );

### <a name="create-a-resource"></a>Create a resource

    Data2CRMAPI.createContactEntity(
        {
            firstname: 'test first name',
            lastname: 'test last name'
        },
        {
            headers: {
                'Accept': 'application/json',
                'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
            }
        },
        function(err, contact, response) {
            if (err) {
                if (err.status >= 400 && err.status <= 499) {
                    console.log('Client error ' + err.status + ': ', err.message);
                } else if (err.status >= 500 && err.status <= 599) {
                    console.log('Server error ' + err.status + ': ', err.message);
                } else {
                    console.log('Unknown error ' + err.status + ': ', err.message);
                }
            } else {
                console.log('Create contact result:', contact);
            }
        }
    );

### <a name="update-a-resource"></a>Update a resource

    Data2CRMAPI.updateContactEntity(
        '003580000021sqUAAQ',
        {
            firstname: 'test2 first name',
            lastname: 'test2 last name'
        },
        {
            headers: {
                'Accept': 'application/json',
                'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
            }
        },
        function(err, contact, response) {
            if (err) {
                if (err.status >= 400 && err.status <= 499) {
                    console.log('Client error ' + err.status + ': ', err.message);
                } else if (err.status >= 500 && err.status <= 599) {
                    console.log('Server error ' + err.status + ': ', err.message);
                } else {
                    console.log('Unknown error ' + err.status + ': ', err.message);
                }
            } else {
                console.log('Update contact result:', contact);
            }
        }
    );

### <a name="delete-a-resource"></a>Delete a resource

    Data2CRMAPI.deleteContactEntity(
        '003580000021sqUAAQ',
        {
            headers: {
                'Accept': 'application/json',
                'X-API2CRM-CRMKEY': '<CRM_KEY>' // change <CRM_KEY> to your CRM key
            }
        },
        function(err, contact, response) {
            if (err) {
                if (err.status >= 400 && err.status <= 499) {
                    console.log('Client error ' + err.status + ': ', err.message);
                } else if (err.status >= 500 && err.status <= 599) {
                    console.log('Server error ' + err.status + ': ', err.message);
                } else {
                    console.log('Unknown error ' + err.status + ': ', err.message);
                }
            } else {
                console.log('Delete contact success');
            }
        }
    );