'use strict';

var Data2CRMAPI = require('./sdks/data2CRMAPI');
var restletUtils = require('./restletUtils');

module.exports = {
  Data2CRMAPI : Data2CRMAPI,
  restletUtils: restletUtils
};